-------------------------------
---CREATE FUNCTION dq_column_values TO QUERY COLUMN VALUES STATISTIC
-------------------------------
------------
-- Variable definitions:
------------
-- table_schema_ varchar: schema name - for example: 'public'
-- table_name_constr varchar: this is a wildcard value to constraint the list of concerning tables. 
-- For example: table_name_constr can be a single table_name 'areas';
-- or list of table with first letter in the range from a to z: '[a-z]%'
------------
-- Output of function:
------------
-- Function will return a table of all values;

CREATE 
or replace FUNCTION dq_column_values( table_schema_ varchar, table_name_constr text ) 
RETURNS table ( tablename text, columnname text, example_value text, count_uniques bigint, list_unique_values text[], max_value text, min_value text ) language plpgsql AS $$ 
DECLARE cmd text;
BEGIN
   for cmd in 
   select
      format( ' 
 
      SELECT
         % L as table_name, KEY AS COLUMN_NAME, max(value) AS example_value, count(DISTINCT value) AS count_uniques, 
         CASE
            WHEN
               count(DISTINCT value) > 0 
               AND count(DISTINCT value) <= 100 
            THEN
               array_agg(DISTINCT value) 
            ELSE
               NULL 
         END
         AS list_unique_values, max(value) AS max_value, min(value) AS min_value 
      FROM
         (
            SELECT
               row_to_json (t.*) AS LINE 
            FROM
               mdm.' || quote_ident(table_name)|| ' t 
         )
         AS r CROSS 
         JOIN
            LATERAL json_each_text (r.line) 
      GROUP BY
         table_name, COLUMN_NAME;
', 
 table_name ) as script 
      from
         (
            select distinct
               table_name,
               table_schema 
            from
               information_schema.columns 
            where
               table_schema = table_schema_ 
               and table_name similar to table_name_constr 
         )
         t 
loop  
	RETURN QUERY EXECUTE cmd;
end loop;
END;
$$ ;

-- EXECUTE FUNCTION EXAMPLE:
-- Query the column values of all tables in schema 'public' which have their name's first letter in the alphabet range from e to z:
select * from dq_column_values('public', '[e-z]%');
